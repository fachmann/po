import {RouterModule, Routes} from '@angular/router';
import {ModuleWithProviders} from '@angular/core';

export const routes: Routes = [
  {path: 'plan', loadChildren: 'app/plan/plan.module#PlanModule', data: {preload: true}},
  {path: '', redirectTo: 'plan', pathMatch: 'full'},
  {path: '**', redirectTo: 'plan/browse', pathMatch: 'full'}
];

export const AppRoutes: ModuleWithProviders = RouterModule.forRoot(routes);
