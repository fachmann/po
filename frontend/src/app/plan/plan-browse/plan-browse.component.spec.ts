import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {Injectable} from "@angular/core";
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/do';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/first';

import {PlanBrowseComponent} from './plan-browse.component';
import {PlanService} from "../plan.service";
import {Model} from "../../model/model";
import Semester = Model.Semester;
import AcademicTeacher = Model.AcademicTeacher;
import Plan = Model.Plan;
import Course = Model.Course;
import Auditing = Model.Auditing;

import {CommonModule} from '@angular/common';
import {PlanEditComponent} from '../plan-edit/plan-edit.component';
import {PlanDetailsComponent} from '../plan-details/plan-details.component';
import {RouterModule} from '@angular/router';
import {HttpClientModule} from '@angular/common/http';

import {
  AccordionModule,
  AutoCompleteModule,
  ButtonModule,
  CheckboxModule,
  DataListModule,
  DataTableModule,
  DropdownModule,
  GrowlModule,
  InputSwitchModule,
  InputTextModule,
  MessageModule,
  MessagesModule,
  PanelModule,
  SharedModule,
  ToggleButtonModule,
  TooltipModule
} from 'primeng/primeng';
import {FormsModule} from '@angular/forms';
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {By} from "@angular/platform-browser";
import {RouterTestingModule} from "@angular/router/testing";


describe('PlanBrowseComponent', () => {
  let component: PlanBrowseComponent;
  let fixture: ComponentFixture<PlanBrowseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        CommonModule,
        RouterModule.forChild([
          {path: 'details/:id', component: PlanDetailsComponent},
          {path: 'edit/:id', component: PlanEditComponent},
          {path: 'browse', component: PlanBrowseComponent},
          {path: '', component: PlanBrowseComponent}
        ]),
        HttpClientModule,
        RouterTestingModule,
        FormsModule,

        AutoCompleteModule,
        MessageModule,
        MessagesModule,
        InputTextModule,
        ButtonModule,
        DataTableModule,
        SharedModule,
        DropdownModule,
        TooltipModule,
        CheckboxModule,
        PanelModule,
        InputSwitchModule,
        DataListModule,
        ToggleButtonModule,
        AccordionModule,
        GrowlModule,

        BrowserAnimationsModule
      ],
      providers: [
        {provide: PlanService, useValue: new MockPlanService()},
      ],
      declarations: [PlanEditComponent, PlanDetailsComponent, PlanBrowseComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PlanBrowseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    // then
    expect(component).toBeTruthy();
  });

  it('should display message saying "Filtruj"', () => {
    fixture.detectChanges();
    // when
    const element = fixture.debugElement.query(By.css('div div div div div h1'));

    // then
    expect(element.nativeElement.innerText)
      .toBe('Filtruj');
  });

  it('should have plans', () => {
    fixture.detectChanges();
    // when
    const rows = fixture.nativeElement.querySelectorAll('.ui-datatable-data .ui-widget-content');

    // then
    expect(component.filteredPlans.length)
      .toBe(3);
    expect(rows.length)
      .toBe(3);
  });

  it('should print plan status properly', () => {
    fixture.detectChanges();
    // when
    let rows = fixture.debugElement.queryAll(By.css('.ui-datatable-data .ui-widget-content'));

    // then
    expect(component.filteredPlans.length)
      .toBe(3);
    expect(rows.length)
      .toBe(3);
    expect(rows[0].query(By.css('td:nth-child(2) span')).nativeElement.innerText)
      .toBe('Zaakceptowany');
    expect(rows[1].query(By.css('td:nth-child(2) span')).nativeElement.innerText)
      .toBe('Odrzucony');
    expect(rows[2].query(By.css('td:nth-child(2) span')).nativeElement.innerText)
      .toBe('Niezweryfikowany');
  });

  it('should print plan semester properly', () => {
    fixture.detectChanges();
    // when
    let rows = fixture.debugElement.queryAll(By.css('.ui-datatable-data .ui-widget-content'));

    // then
    expect(component.filteredPlans.length)
      .toBe(3);
    expect(rows.length)
      .toBe(3);
    expect(rows[0].query(By.css('td:nth-child(3) span')).nativeElement.innerText)
      .toBe('Zimowy 16/17');
    expect(rows[1].query(By.css('td:nth-child(3) span')).nativeElement.innerText)
      .toBe('Letni 16/17');
    expect(rows[2].query(By.css('td:nth-child(3) span')).nativeElement.innerText)
      .toBe('Letni 16/17');
  });

  it('should count auditings properly', () => {
    fixture.detectChanges();
    // when
    let rows = fixture.debugElement.queryAll(By.css('.ui-datatable-data .ui-widget-content'));

    // then
    expect(component.filteredPlans.length)
      .toBe(3);
    expect(rows.length)
      .toBe(3);
    expect(rows[0].query(By.css('td:nth-child(4) span')).nativeElement.innerText)
      .toBe('0');
    expect(rows[1].query(By.css('td:nth-child(4) span')).nativeElement.innerText)
      .toBe('0');
    expect(rows[2].query(By.css('td:nth-child(4) span')).nativeElement.innerText)
      .toBe('4');
  });

  it('should filter by document number when empty filter', () => {
    fixture.detectChanges();
    // given
    const documentNumberInput = fixture.debugElement.query(By.css('#documentNumber')).nativeElement;
    documentNumberInput.value = '';
    documentNumberInput.dispatchEvent(new Event('input'));

    // when
    component.filter();
    fixture.detectChanges();

    // then
    expect(component.filterOptions.documentNumber)
      .toBe('');
    expect(component.filteredPlans.length)
      .toBe(3);
    expect(fixture.debugElement.queryAll(By.css('.ui-datatable-data .ui-widget-content')).length)
      .toBe(3);
  });

  it('should filter by document number when no documents matches', () => {
    fixture.detectChanges();
    // given
    const documentNumberInput = fixture.debugElement.query(By.css('#documentNumber')).nativeElement;
    documentNumberInput.value = 'z';
    documentNumberInput.dispatchEvent(new Event('input'));

    // when
    component.filter();
    fixture.detectChanges();

    // then
    expect(component.filterOptions.documentNumber)
      .toBe('z');
    expect(component.filteredPlans.length)
      .toBe(0);
    expect(fixture.debugElement.query(By.css('.ui-datatable-emptymessage .ng-star-inserted')).nativeElement.innerText)
      .toBe('Brak planów hospitacji');

  });

  it('should filter by document number when full result', () => {
    fixture.detectChanges();
    // given
    const documentNumberInput = fixture.debugElement.query(By.css('#documentNumber')).nativeElement;
    documentNumberInput.value = 'A';
    documentNumberInput.dispatchEvent(new Event('input'));

    // when
    component.filter();
    fixture.detectChanges();

    // then
    expect(component.filterOptions.documentNumber)
      .toBe('A');
    expect(component.filteredPlans.length)
      .toBe(3);
    expect(fixture.debugElement.queryAll(By.css('.ui-datatable-data .ui-widget-content')).length)
      .toBe(3);
  });

  it('should filter by document number when partial result', () => {
    fixture.detectChanges();
    // given
    const documentNumberInput = fixture.debugElement.query(By.css('#documentNumber')).nativeElement;
    documentNumberInput.value = 'AE';
    documentNumberInput.dispatchEvent(new Event('input'));

    // when
    component.filter();
    fixture.detectChanges();
    let rows = fixture.debugElement.queryAll(By.css('.ui-datatable-data .ui-widget-content'));

    // then
    expect(component.filterOptions.documentNumber)
      .toBe('AE');
    expect(component.filteredPlans.length)
      .toBe(2);
    expect(rows.length)
      .toBe(2);
    expect(rows[0].query(By.css('td span a')).nativeElement.innerText)
      .toBe('AEA/001');
    expect(rows[1].query(By.css('td span a')).nativeElement.innerText)
      .toBe('AEF/003');
  });

  it('should filter by document number when one result', () => {
    fixture.detectChanges();
    // given
    const documentNumberInput = fixture.debugElement.query(By.css('#documentNumber')).nativeElement;
    documentNumberInput.value = 'AEA';
    documentNumberInput.dispatchEvent(new Event('input'));

    // when
    component.filter();
    fixture.detectChanges();
    let rows = fixture.debugElement.queryAll(By.css('.ui-datatable-data .ui-widget-content'));

    // then
    expect(component.filterOptions.documentNumber)
      .toBe('AEA');
    expect(component.filteredPlans.length)
      .toBe(1);
    expect(rows.length)
      .toBe(1);
    expect(rows[0].query(By.css('td span a')).nativeElement.innerText)
      .toBe('AEA/001');
  });

  it('should filter by semester when any semester', () => {
    fixture.detectChanges();
    // given
    component.filterOptions.semester = component.semesters[0].value;

    // when
    component.filter();
    fixture.detectChanges();
    let rows = fixture.debugElement.queryAll(By.css('.ui-datatable-data .ui-widget-content'));

    // then
    expect(component.filteredPlans.length)
      .toBe(3);
    expect(rows.length)
      .toBe(3);
  });

  it('should filter by semester when partial result', () => {
    fixture.detectChanges();
    // given
    component.filterOptions.semester = component.semesters[2].value;

    // when
    component.filter();
    fixture.detectChanges();
    let rows = fixture.debugElement.queryAll(By.css('.ui-datatable-data .ui-widget-content'));

    // then
    expect(component.filteredPlans.length)
      .toBe(2);
    expect(rows.length)
      .toBe(2);
    expect(rows[0].query(By.css('td span a')).nativeElement.innerText)
      .toBe('ABA/002');
    expect(rows[1].query(By.css('td span a')).nativeElement.innerText)
      .toBe('AEF/003');
  });

  it('should filter by semester when no result', () => {
    fixture.detectChanges();
    // given
    component.filterOptions.semester = component.semesters[4].value;

    // when
    component.filter();
    fixture.detectChanges();

    // then
    expect(component.filteredPlans.length)
      .toBe(0);
    expect(fixture.debugElement.query(By.css('.ui-datatable-emptymessage .ng-star-inserted')).nativeElement.innerText)
      .toBe('Brak planów hospitacji');
  });

  it('should filter by semester and documend number combined', () => {
    fixture.detectChanges();
    // given
    component.filterOptions.semester = component.semesters[2].value;

    const documentNumberInput = fixture.debugElement.query(By.css('#documentNumber')).nativeElement;
    documentNumberInput.value = 'AE';
    documentNumberInput.dispatchEvent(new Event('input'));

    // when
    component.filter();
    fixture.detectChanges();
    let rows = fixture.debugElement.queryAll(By.css('.ui-datatable-data .ui-widget-content'));

    // then
    expect(component.filterOptions.documentNumber)
      .toBe('AE');
    expect(component.filteredPlans.length)
      .toBe(1);
    expect(rows.length)
      .toBe(1);
    expect(rows[0].query(By.css('td span a')).nativeElement.innerText)
      .toBe('AEF/003');
  });

  it('should reset filter', () => {
    fixture.detectChanges();
    // given
    component.filterOptions.semester = component.semesters[2].value;

    const documentNumberInput = fixture.debugElement.query(By.css('#documentNumber')).nativeElement;
    documentNumberInput.value = 'AE';
    documentNumberInput.dispatchEvent(new Event('input'));

    component.filter();
    fixture.detectChanges();
    let rows = fixture.debugElement.queryAll(By.css('.ui-datatable-data .ui-widget-content'));

    expect(component.filterOptions.documentNumber)
      .toBe('AE');
    expect(component.filteredPlans.length)
      .toBe(1);
    expect(rows.length)
      .toBe(1);
    expect(rows[0].query(By.css('td span a')).nativeElement.innerText)
      .toBe('AEF/003');

    const resetButton = fixture.debugElement.query(By.css('button[label="Reset filtra"]')).nativeElement;

    // when
    resetButton.click();
    fixture.detectChanges();

    // then
    expect(component.filterOptions.documentNumber)
      .toBe('');
    expect(component.filterOptions.semester.id)
      .toBe(-1);
    expect(component.filteredPlans.length)
      .toBe(3);
    expect(fixture.debugElement.queryAll(By.css('.ui-datatable-data .ui-widget-content')).length)
      .toBe(3);
  });
});


@Injectable()
class MockPlanService extends PlanService {

  constructor() {
    super(null);
  }

  getSemesters(): Observable<Semester[]> {
    return Observable.of(semesters);
  }

  getTeachers(): Observable<AcademicTeacher[]> {
    return Observable.of(null);
  }

  getCoursesByTeacher(teacher: AcademicTeacher): Observable<Course[]> {
    return this.getCoursesByTeacherId(teacher.id)
  }

  getCoursesByTeacherId(teacherId: number): Observable<Course[]> {
    return Observable.of(null);
  }

  savePlan(plan: Plan): Observable<Plan> {
    return Observable.of(null);
  }

  getPlan(id: number): Promise<Plan> {
    const filteredPlans = plans.filter(value => value.id === id);
    return Observable.of(filteredPlans.length > 0 ? filteredPlans[0] : null).toPromise();
  }

  getPlans(): Observable<Plan[]> {
    return Observable.of(plans);
  }
}


let semesters: Semester[] = [
  new Semester(0, 'WINTER', 2016),
  new Semester(1, 'SUMMER', 2016),
  new Semester(2, 'WINTER', 2017),
  new Semester(3, 'SUMMER', 2017)
];

let emptyAuditing: Auditing = {
  id: 0,
  audited: null,
  committee: null,
  course: null,
  dateOfCompletion: null,
  status: "COMPLEX",
};

let plans: Plan[] = [
  {
    id: 0,
    status: 'ACCEPTED',
    documentNumber: 'AEA/001',
    semester: semesters[0],
    auditing: []
  },
  {
    id: 1,
    status: 'REJECTED',
    documentNumber: 'ABA/002',
    semester: semesters[1],
    auditing: null
  },
  {
    id: 1,
    status: 'NOT_EXAMINED',
    documentNumber: 'AEF/003',
    semester: semesters[1],
    auditing: [
      emptyAuditing, emptyAuditing, emptyAuditing, emptyAuditing
    ]
  }
];
