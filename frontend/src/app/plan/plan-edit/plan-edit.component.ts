import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {PlanService} from '../plan.service';
import {Message, SelectItem} from 'primeng/primeng';
import {Model} from "../../model/model";
import Plan = Model.Plan;
import Committee = Model.Committee;
import AcademicTeacher = Model.AcademicTeacher;
import Semester = Model.Semester;
import Auditing = Model.Auditing;
import Course = Model.Course;

@Component({
  selector: 'app-model-search',
  templateUrl: './plan-edit.component.html',
  styleUrls: ['./plan-edit.component.scss']
})
export class PlanEditComponent implements OnInit {
  //growl
  messages: Message[] = [];
  messagesPermanent: Message[] = [];

  // downloaded
  plan: Plan = null;
  teachersItems: SelectItem[] = null;
  teacherCourseItems: SelectItem[] = null;

  filteredTeachersItems: SelectItem[] = null;

  // add auditing
  newTeacherItem: any = null;
  newCourseItem: any = null;
  newCommittee: Committee = {members: []};
  newMemberItem: any = null;

  academicTeacherCanBeChairman = AcademicTeacher.canBeChairman;
  academicTeacherToString = AcademicTeacher.toString;
  semesterToString = Semester.toString;


  constructor(private route: ActivatedRoute, private planService: PlanService) {
  }

  ngOnInit() {
    window.scrollTo(0, 0);
    this.route.params.subscribe(params => {
      const id = Number(params.id);
      if (!isNaN(id)) {
        this.getPlan(id);
      }
    });
  }

  searchTeacher(event: any, actualMembers: AcademicTeacher[] = []): void {
    this.filteredTeachersItems = [...this.teachersItems.filter(
      teacher => teacher.label.toLowerCase().includes(event.query.toLowerCase()) &&
        !actualMembers.map(member => member.id).includes(teacher.value.id)
    )];
  }

  addMember(committee: Committee, teacherItem: SelectItem, row: any = null) {
    if (teacherItem instanceof Object) {
      const teacher = teacherItem.value;
      if (committee.members.indexOf(teacher) === -1) {
        committee.members.push(teacher);
        if (row) {
          row.newMemberItem = '';
        } else {
          this.newMemberItem = null;
        }
      }
    }
  }

  setChairman(committee: Committee, member: AcademicTeacher) {
    committee.chairman = new AcademicTeacher(-1, null, null, null);
    setTimeout(() => {
      committee.chairman = member;
    }, 1);
  }

  deleteMember(committee: Committee, member: AcademicTeacher) {
    const index = committee.members.indexOf(member);
    committee.members.splice(index, 1);
    if (committee.chairman != null && member.id === committee.chairman.id) {
      committee.chairman = null;
    }
  }

  addAuditing() {
    const newAuditing: Auditing = {
      course: this.newCourseItem,
      audited: this.newTeacherItem.value,
      committee: this.newCommittee
    };
    this.updateCourseDates(newAuditing.course, newAuditing.audited);
    console.log(newAuditing);
    this.plan.auditing.push(newAuditing);
    this.plan.auditing = [...this.plan.auditing];
    this.teacherCourseItems = null;
    this.newTeacherItem = null;
    this.newCourseItem = null;
    this.newCommittee = {members: []};
    this.newMemberItem = null;
  }

  deleteAuditing(index): void {
    this.plan.auditing = this.plan.auditing.filter((value, i) => i !== index);
  }

  updateTeacherCourses(event: any) {
    if (this.newTeacherItem && this.newTeacherItem.value && this.newTeacherItem.value.id) {
      this.planService.getCoursesByTeacher(this.newTeacherItem.value).subscribe(courses =>
        this.teacherCourseItems = courses.map(course => ({
          label: course.name + ' - ' + course.code,
          value: course
        }))
      );
    } else {
      this.teacherCourseItems = null;
      this.newCourseItem = null;
    }
  }

  updateCourseDates(course: Course, academicTeacher: AcademicTeacher) {
    var dates = [];
    course.groups
      .filter(group => group.academicTeacher.id == academicTeacher.id)
      .forEach(group => dates = dates.concat(group.dates));
    course.availableDates = dates;
  }

  formatDate(date: Date): string {
    date = new Date(date);
    if (date) {
      return date.getDate() + '.' + (date.getMonth() + 1) + '.' + date.getFullYear() + ' - '
        + date.getHours() + ':' + date.getMinutes();
    }
    return '';
  }

  onResize(event): void {

  }

  save(event: Event) {
    this.planService.savePlan(this.plan).subscribe(plan => {
      console.log(plan);
      this.update(plan);
      this.messages.push({
        severity: 'success',
        summary: 'Zapisano',
        detail: 'Plan nr ' + plan.documentNumber + ' został zapisany.'
      });
      setTimeout(() => this.messages = [], 7000);
    });
  }

  update(plan: Plan) {
    this.plan = plan;
    this.plan.auditing.forEach(auditing => this.updateCourseDates(auditing.course, auditing.audited));
  }

  private getPlan(id: number): void {
    this.plan = null;
    this.messages = [];
    this.messagesPermanent = [];
    this.planService.getTeachers().subscribe(teachers => {
      this.teachersItems = teachers.map(teacher => ({
          label: AcademicTeacher.toString(teacher),
          value: teacher
        })
      );
      this.planService.getPlan(id).then(plan => {
        this.update(plan);
      }).catch(reason => {
        console.log(reason);
        if (reason.error === "ENTITY_NOT_FOUND") {
          this.messagesPermanent.push({
            severity: 'error',
            summary: 'Błąd',
            detail: 'Plan nie istnieje.'
          });
        } else {
          this.messagesPermanent.push({
            severity: 'error',
            summary: 'Błąd',
            detail: reason.message
          })
        }
      });
    });
  }

  validatePlan(): boolean {
    return this.plan == null ||
      this.plan.auditing == null ||
      this.plan.auditing.length <= 0 ||
      !this.plan.auditing.every(auditing => auditing != null &&
        auditing.committee != null &&
        auditing.committee.chairman != null);
  }
}
