import {Injectable} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/do';
import 'rxjs/add/observable/of';
import {HttpClient} from '@angular/common/http';
import 'rxjs/add/operator/first';
import {Model} from "../model/model";
import Semester = Model.Semester;
import AcademicTeacher = Model.AcademicTeacher;
import Plan = Model.Plan;
import Course = Model.Course;

@Injectable()
export class PlanService {

  endPoint = 'http://localhost:8080/api';

  constructor(private http: HttpClient) {
  }

  getSemesters(): Observable<Semester[]> {
    return this.http.get<Semester[]>(this.endPoint + '/plan/semesters');
  }

  getTeachers(): Observable<AcademicTeacher[]> {
    return this.http.get<AcademicTeacher[]>(this.endPoint + '/teacher/teachers');
  }

  getCoursesByTeacher(teacher: AcademicTeacher): Observable<Course[]> {
    return this.getCoursesByTeacherId(teacher.id)
  }

  getCoursesByTeacherId(teacherId: number): Observable<Course[]> {
    return this.http.get<Course[]>(this.endPoint + '/course/coursesByTeacherId/' + teacherId);
  }

  getPlan(id: number): Promise<Plan> {
    return this.http.get<Plan>(this.endPoint + '/plan/plan/' + id).toPromise();
  }

  savePlan(plan: Plan): Observable<Plan> {
    return this.http.post<Plan>(this.endPoint + '/plan/save', plan);
  }

  getPlans(): Observable<Plan[]> {
    return this.http.get<Plan[]>(this.endPoint + '/plan/plans');
  }
}
