import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {PlanDetailsComponent} from './plan-details.component';

import {Injectable} from "@angular/core";
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/do';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/first';

import {PlanService} from "../plan.service";
import {Model} from "../../model/model";
import Semester = Model.Semester;
import AcademicTeacher = Model.AcademicTeacher;
import Plan = Model.Plan;
import Course = Model.Course;
import Auditing = Model.Auditing;

import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {PlanEditComponent} from '../plan-edit/plan-edit.component';
import {PlanBrowseComponent} from '../plan-browse/plan-browse.component';
import {RouterModule} from '@angular/router';
import {HttpClientModule} from '@angular/common/http';

import {
  AccordionModule,
  AutoCompleteModule,
  ButtonModule,
  CheckboxModule,
  DataListModule,
  DataTableModule,
  DropdownModule,
  GrowlModule,
  InputSwitchModule,
  InputTextModule,
  MessageModule,
  MessagesModule,
  PanelModule,
  SharedModule,
  ToggleButtonModule,
  TooltipModule
} from 'primeng/primeng';
import {FormsModule} from '@angular/forms';
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {RouterTestingModule} from "@angular/router/testing";



describe('PlanDetailsComponent', () => {
  let component: PlanDetailsComponent;
  let fixture: ComponentFixture<PlanDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({imports: [
      CommonModule,
      RouterTestingModule,
      HttpClientModule,
      FormsModule,

      AutoCompleteModule,
      MessageModule,
      MessagesModule,
      InputTextModule,
      ButtonModule,
      DataTableModule,
      SharedModule,
      DropdownModule,
      TooltipModule,
      CheckboxModule,
      PanelModule,
      InputSwitchModule,
      DataListModule,
      ToggleButtonModule,
      AccordionModule,
      GrowlModule,

      BrowserAnimationsModule
    ],
      providers: [
        PlanService
      ],
      declarations: [PlanEditComponent, PlanDetailsComponent, PlanBrowseComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PlanDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
