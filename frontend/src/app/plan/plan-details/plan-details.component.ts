import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {PlanService} from '../plan.service';
import {Model} from "../../model/model";
import {Message} from "primeng/primeng";
import Plan = Model.Plan;
import Semester = Model.Semester;

@Component({
  selector: 'app-plan-details',
  templateUrl: './plan-details.component.html',
  styleUrls: ['./plan-details.component.scss']
})
export class PlanDetailsComponent implements OnInit {
  //growl
  messagesPermanent: Message[] = [];

  plan: Plan = null;

  semesterToString = Semester.toString;

  constructor(private route: ActivatedRoute, private planService: PlanService) {
  }

  ngOnInit() {
    window.scrollTo(0, 0);
    this.route.params.subscribe(params => {
      const id = Number(params.id);
      if (!isNaN(id)) {
        this.getPlan(id);
      }
    });
  }

  private getPlan(id: number): void {
    this.plan = null;
    this.messagesPermanent = [];
    this.planService.getPlan(id).then(plan => {
      this.plan = plan;
    }).catch(reason => {
      if (reason.error === "ENTITY_NOT_FOUND") {
        this.messagesPermanent.push({
          severity: 'error',
          summary: 'Błąd',
          detail: 'Plan nie istnieje.'
        });
      } else {
        this.messagesPermanent.push({
          severity: 'error',
          summary: 'Błąd',
          detail: reason.message
        })
      }
    });
  }
}
