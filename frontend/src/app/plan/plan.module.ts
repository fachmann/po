import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {PlanEditComponent} from './plan-edit/plan-edit.component';
import {PlanDetailsComponent} from './plan-details/plan-details.component';
import {PlanBrowseComponent} from './plan-browse/plan-browse.component';
import {RouterModule} from '@angular/router';
import {HttpClientModule} from '@angular/common/http';
import {PlanService} from './plan.service';

import {
  AccordionModule,
  AutoCompleteModule,
  ButtonModule,
  CheckboxModule,
  DataListModule,
  DataTableModule,
  DropdownModule,
  GrowlModule,
  InputSwitchModule,
  InputTextModule,
  MessageModule,
  MessagesModule,
  PanelModule,
  SharedModule,
  ToggleButtonModule,
  TooltipModule
} from 'primeng/primeng';
import {FormsModule} from '@angular/forms';


/**
 * The PlanModule contains all functionalities realted to Plan subdomain.
 */
@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild([
      {path: 'details/:id', component: PlanDetailsComponent},
      {path: 'edit/:id', component: PlanEditComponent},
      {path: 'browse', component: PlanBrowseComponent},
      {path: '', component: PlanBrowseComponent}
    ]),
    HttpClientModule,
    FormsModule,

    AutoCompleteModule,
    MessageModule,
    MessagesModule,
    InputTextModule,
    ButtonModule,
    DataTableModule,
    SharedModule,
    DropdownModule,
    TooltipModule,
    CheckboxModule,
    PanelModule,
    InputSwitchModule,
    DataListModule,
    ToggleButtonModule,
    AccordionModule,
    GrowlModule
  ],
  providers: [
    PlanService
  ],
  declarations: [PlanEditComponent, PlanDetailsComponent, PlanBrowseComponent]
})
export class PlanModule {
}
