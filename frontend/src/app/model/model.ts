export module Model {
  export class AcademicTeacher {
    id?: number;
    degree: AcademicDegree;
    name: string;
    surname: string;

    constructor(id: number, degree: AcademicDegree, name: string, surname: string) {
      this.id = id;
      this.degree = degree;
      this.name = name;
      this.surname = surname;
    }

    static toString(instance: AcademicTeacher): string {
      switch (instance.degree) {
        case 'ENGINEER':
          return 'inż.' + ' ' + instance.name + ' ' + instance.surname;
        case 'MASTER':
          return 'mgr' + ' ' + instance.name + ' ' + instance.surname;
        case 'DOCTOR':
          return 'dr' + ' ' + instance.name + ' ' + instance.surname;
        case 'DOCTOR_ENGINEER':
          return 'dr inż.' + ' ' + instance.name + ' ' + instance.surname;
        case 'HABILITATION':
          return 'dr hab.' + ' ' + instance.name + ' ' + instance.surname;
        case 'ASSOCIATE_PROFESSOR':
          return 'prof. nadzw.' + ' ' + instance.name + ' ' + instance.surname;
        case 'PROFESSOR':
          return 'prof.' + ' ' + instance.name + ' ' + instance.surname;
      }
    }

    static canBeChairman(instance: AcademicTeacher): boolean {
      return instance.degree === 'PROFESSOR' || instance.degree === 'ASSOCIATE_PROFESSOR' || instance.degree === 'HABILITATION';
    }
  }

  export class Committee {
    id?: number;
    chairman?: AcademicTeacher;
    members: AcademicTeacher[];
  }

  export class Auditing {
    id?: number;
    dateOfCompletion?: Date;
    status?: AuditingStatus = 'COMPLEX';
    audited: AcademicTeacher;
    committee: Committee;
    course: Course;
  }

  export class Course {
    id?: number;
    name: string;
    code: string;
    groups: Group[];
    availableDates: Date[];
  }

  export class Group {
    id?: number;
    code: string;
    places: number;
    totalPlaces: number;
    room: string;
    building: string;
    dates: Date[];
    academicTeacher: AcademicTeacher;
  }

  export class Document {
    id?: number;
    documentNumber: string;
  }

  export class Semester {
    id?: number;
    type: SemesterType;
    beginYear: number;

    constructor(id: number, type: SemesterType, beginYear: number) {
      this.id = id;
      this.type = type;
      this.beginYear = beginYear;
    }

    get endYear(): number {
      return this.beginYear + 1;
    }

    static toString(instance: Semester): string {
      switch (instance.type) {
        case 'SUMMER':
          return 'Letni ' + (instance.beginYear % 100) + '/' + (instance.endYear % 100);
        case 'WINTER':
          return 'Zimowy ' + (instance.beginYear % 100) + '/' + (instance.endYear % 100);
      }
    }
  }

  export class Plan extends Document {
    status: PlanStatus = 'NOT_EXAMINED';
    auditing: Auditing[];
    semester: Semester;
  }


  export type AuditingStatus =
    'COMPLEX'
    | 'EVALUATED'
    | 'ACCEPTED'
    | 'REJECTED'
    | 'FINISHED';

  export type Grade =
    'DISTINCTIVELY'
    | 'WELL'
    | 'ENOUGH'
    | 'INSUFFICIENTLY'
    | 'NOT_APPLICABLE';

  export type AcademicDegree =
    'ENGINEER'
    | 'MASTER'
    | 'DOCTOR'
    | 'DOCTOR_ENGINEER'
    | 'HABILITATION'
    | 'ASSOCIATE_PROFESSOR'
    | 'PROFESSOR';

  export type SemesterType = 'WINTER' | 'SUMMER';

  export type PlanStatus = 'ACCEPTED' | 'NOT_EXAMINED' | 'REJECTED';
}
