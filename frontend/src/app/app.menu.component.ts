import {Component, forwardRef, Inject, Input, OnInit, ViewEncapsulation} from '@angular/core';
import {animate, state, style, transition, trigger} from '@angular/animations';
import {Location} from '@angular/common';
import {Router} from '@angular/router';
import {MenuItem} from 'primeng/primeng';
import {AppComponent} from './app.component';

@Component({
  selector: 'app-menu',
  templateUrl: './app.menu.component.html',
  styleUrls: ['./app.component.css'],
  encapsulation: ViewEncapsulation.None

})
export class AppMenuComponent implements OnInit {

  @Input() reset: boolean;

  model: MenuItem[];

  constructor(@Inject(forwardRef(() => AppComponent)) public app: AppComponent) {
  }

  ngOnInit() {
    this.model = [
      {label: 'Przeglądaj plany hospitacji', icon: 'fa fa-fw fa-database', routerLink: ['/', 'plan', 'browse']},
      {
        label: 'Utwórz plan hospitacji',
        icon: 'fa fa-fw fa-file-text',
        routerLink: ['/'],
        badgeStyleClass: 'fnn-disabled'
      },
      {
        label: 'Przeglądaj protokoły z hospitacji',
        icon: 'fa fa-fw fa-book',
        routerLink: ['/'],
        badgeStyleClass: 'fnn-disabled'
      }
    ];
  }

  changeTheme(theme) {
    const themeLink: HTMLLinkElement = <HTMLLinkElement> document.getElementById('theme-css');
    themeLink.href = 'assets/theme/theme-' + theme + '.css';
  }

  changeLayout(layout) {
    this.app.layout = layout;
    const layoutLink: HTMLLinkElement = <HTMLLinkElement> document.getElementById('layout-css');
    layoutLink.href = 'assets/layout/css/layout-' + layout + '.css';
  }
}

@Component({
  selector: 'app-submenu',
  templateUrl: './app.submenu.component.html',
  animations: [
    trigger('children', [
      state('hiddenAnimated', style({
        height: '0px'
      })),
      state('visibleAnimated', style({
        height: '*'
      })),
      state('visible', style({
        height: '*'
      })),
      state('hidden', style({
        height: '0px'
      })),
      transition('visibleAnimated => hiddenAnimated', animate('400ms cubic-bezier(0.86, 0, 0.07, 1)')),
      transition('hiddenAnimated => visibleAnimated', animate('400ms cubic-bezier(0.86, 0, 0.07, 1)'))
    ])
  ]
})
export class AppSubMenuComponent {

  @Input() item: MenuItem;

  @Input() root: boolean;

  @Input() visible: boolean;
  activeIndex: number;
  hover: boolean;

  constructor(@Inject(forwardRef(() => AppComponent)) public app: AppComponent, public router: Router, public location: Location) {
  }

  _reset: boolean;

  @Input()
  get reset(): boolean {
    return this._reset;
  }

  set reset(val: boolean) {
    this._reset = val;

    if (this._reset && this.app.slimMenu) {
      this.activeIndex = null;
    }
  }

  itemClick(event: Event, item: MenuItem, index: number) {
    // avoid processing disabled items
    if (item.disabled) {
      event.preventDefault();
      return true;
    }

    // activate current item and deactivate active sibling if any
    if (item.routerLink || item.items) {
      this.activeIndex = (this.activeIndex === index) ? null : index;
    }

    // execute command
    if (item.command) {
      item.command({originalEvent: event, item: item});
    }

    // prevent hash change
    if (item.items || (!item.url && !item.routerLink)) {
      event.preventDefault();
    }

    // hide menu
    if (!item.items) {
      if (this.app.overlayMenu || this.app.isMobile()) {
        this.app.overlayMenuActive = false;
        this.app.mobileMenuActive = false;
      }

      if (!this.root && this.app.slimMenu) {
        this.app.resetSlim = true;
      }
    }
  }

  isActive(index: number): boolean {
    return this.activeIndex === index;
  }

  unsubscribe(item: any) {
    if (item.eventEmitter) {
      item.eventEmitter.unsubscribe();
    }

    if (item.items) {
      for (const childItem of item.items) {
        this.unsubscribe(childItem);
      }
    }
  }
}
