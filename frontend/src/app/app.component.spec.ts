import {async, TestBed} from '@angular/core/testing';

import {AppComponent} from './app.component';
import {AppTopBarComponent} from "./app.topbar.component";
import {AppMenuComponent, AppSubMenuComponent} from "./app.menu.component";
import {AppFooterComponent} from "./app.footer.component";
import {RouterTestingModule} from "@angular/router/testing";

describe('AppComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule.withRoutes([
          {path: '', component: AppComponent}
        ])
      ],
      declarations: [
        AppComponent, AppTopBarComponent, AppMenuComponent, AppSubMenuComponent, AppFooterComponent
      ],
    }).compileComponents();
  }));

  it('should create the app', async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  }));
});
