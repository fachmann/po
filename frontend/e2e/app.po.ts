import {browser, by, element} from 'protractor';

export class MorpheusPage {
  navigateTo() {
    return browser.get('/');
  }

  getParagraphText() {
    return element(by.css('app-topbar div div a span')).getText();
  }
}
