import {MorpheusPage} from './app.po';

describe('morpheus App', () => {
  let page: MorpheusPage;

  beforeEach(() => {
    page = new MorpheusPage();
  });

  it('should display message saying "Wydział informatyki i zarządzania"', () => {
    page.navigateTo();
    expect<any>(page.getParagraphText()).toEqual('Wydział informatyki i zarządzania');
  });
});
