package lt.ro.fachmann.golabek.backend.model

import org.junit.Assert
import org.junit.Test

import org.junit.Assert.*

class AuditingProtocolTest {
    @Test
    fun `grades sum grades null`() {
        // GIVEN
        val auditingProtocol = AuditingProtocol()
        // WHEN
        val gradesSum = auditingProtocol.overallGrade()
        // THEN
        Assert.assertEquals(0.0, gradesSum, 1e-5)
    }

    @Test
    fun `grades sum grades empty`() {
        // GIVEN
        val auditingProtocol = AuditingProtocol(grades = listOf())
        // WHEN
        val gradesSum = auditingProtocol.overallGrade()
        // THEN
        Assert.assertEquals(0.0, gradesSum, 1e-5)
    }

    @Test
    fun `grades sum grades all correct`() {
        // GIVEN
        val auditingProtocol = AuditingProtocol(grades = listOf(
                AuditingGrade(grade = Grade.DISTINCTIVELY), // 1
                AuditingGrade(grade = Grade.DISTINCTIVELY), // 2
                AuditingGrade(grade = Grade.DISTINCTIVELY), // 3
                AuditingGrade(grade = Grade.DISTINCTIVELY), // 4
                AuditingGrade(grade = Grade.DISTINCTIVELY), // 5
                AuditingGrade(grade = Grade.DISTINCTIVELY), // 6
                AuditingGrade(grade = Grade.DISTINCTIVELY), // 7
                AuditingGrade(grade = Grade.DISTINCTIVELY), // 8
                AuditingGrade(grade = Grade.DISTINCTIVELY), // 9
                AuditingGrade(grade = Grade.DISTINCTIVELY), // 10
                AuditingGrade(grade = Grade.DISTINCTIVELY), // 11
                AuditingGrade(grade = Grade.DISTINCTIVELY), // 12
                AuditingGrade(grade = Grade.DISTINCTIVELY), // 13
                AuditingGrade(grade = Grade.DISTINCTIVELY) // 14
        ))
        // WHEN
        val gradesSum = auditingProtocol.overallGrade()
        // THEN
        Assert.assertEquals(5.0, gradesSum, 1e-5)
    }

    @Test
    fun `grades sum grades average`() {
        // GIVEN
        val auditingProtocol = AuditingProtocol(grades = listOf(
                AuditingGrade(grade = Grade.DISTINCTIVELY), // 1
                AuditingGrade(grade = Grade.DISTINCTIVELY), // 2
                AuditingGrade(grade = Grade.DISTINCTIVELY), // 3
                AuditingGrade(grade = Grade.DISTINCTIVELY), // 4
                AuditingGrade(grade = Grade.DISTINCTIVELY), // 5
                AuditingGrade(grade = Grade.DISTINCTIVELY), // 6
                AuditingGrade(grade = Grade.DISTINCTIVELY), // 7
                AuditingGrade(grade = Grade.NOT_APPLICABLE), // 8
                AuditingGrade(grade = Grade.NOT_APPLICABLE), // 9
                AuditingGrade(grade = Grade.NOT_APPLICABLE), // 10
                AuditingGrade(grade = Grade.NOT_APPLICABLE), // 11
                AuditingGrade(grade = Grade.NOT_APPLICABLE), // 12
                AuditingGrade(grade = Grade.NOT_APPLICABLE), // 13
                AuditingGrade(grade = Grade.NOT_APPLICABLE) // 14
        ))
        // WHEN
        val gradesSum = auditingProtocol.overallGrade()
        // THEN
        Assert.assertEquals(5.0, gradesSum, 1e-5)
    }

    @Test
    fun gradesSumGradesHalf() {
        // GIVEN
        val auditingProtocol = AuditingProtocol(grades = listOf(
                AuditingGrade(grade = Grade.DISTINCTIVELY), // 1
                AuditingGrade(grade = Grade.WELL), // 2
                AuditingGrade(grade = Grade.ENOUGH), // 3
                AuditingGrade(grade = Grade.INSUFFICIENTLY), // 4
                AuditingGrade(grade = Grade.NOT_APPLICABLE), // 5
                AuditingGrade(grade = Grade.NOT_APPLICABLE), // 6
                AuditingGrade(grade = Grade.NOT_APPLICABLE), // 7
                AuditingGrade(grade = Grade.NOT_APPLICABLE), // 8
                AuditingGrade(grade = Grade.NOT_APPLICABLE), // 9
                AuditingGrade(grade = Grade.NOT_APPLICABLE), // 10
                AuditingGrade(grade = Grade.NOT_APPLICABLE), // 11
                AuditingGrade(grade = Grade.NOT_APPLICABLE), // 12
                AuditingGrade(grade = Grade.NOT_APPLICABLE), // 13
                AuditingGrade(grade = Grade.NOT_APPLICABLE) // 14
        ))
        // WHEN
        val gradesSum = auditingProtocol.overallGrade()
        // THEN
        Assert.assertEquals((5.0 + 4.0 + 3.0 + 2.0) / 4.0, gradesSum, 1e-5)
    }

}