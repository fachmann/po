package lt.ro.fachmann.golabek.backend

import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.junit4.SpringRunner

@RunWith(SpringRunner::class)
@SpringBootTest(properties = ["spring.profiles.active=test"])
class BackendApplicationTests {

	@Test
	fun contextLoads() {
	}

}
