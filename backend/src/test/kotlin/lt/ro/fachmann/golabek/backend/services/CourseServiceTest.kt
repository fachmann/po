package lt.ro.fachmann.golabek.backend.services

import org.junit.Assert
import org.junit.Test

import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.junit4.SpringRunner

@RunWith(SpringRunner::class)
@SpringBootTest(properties = ["spring.profiles.active=test"])
class CourseServiceTest {

    @Autowired
    lateinit private var courseService: CourseService

    @Test
    fun findAllGroupsByTeacherNegativeIndex() {
        val findAllGroupsByTeacher = courseService.findAllCoursesByTeacher(-1)
        Assert.assertNotNull("ArrayList not found!", findAllGroupsByTeacher)
        Assert.assertTrue("ArrayList must be empty in this case", findAllGroupsByTeacher.isEmpty())
    }

    @Test
    fun findAllGroupsByTeacherTeacherWithNoGroups() {
        val findAllGroupsByTeacher = courseService.findAllCoursesByTeacher(11)
        Assert.assertNotNull("ArrayList not found!", findAllGroupsByTeacher)
        Assert.assertTrue("ArrayList must be empty in this case", findAllGroupsByTeacher.isEmpty())
    }

    @Test
    fun findAllGroupsByTeacherTeacherWithGroups() {
        val findAllGroupsByTeacher = courseService.findAllCoursesByTeacher(10)
        Assert.assertNotNull("ArrayList not found!", findAllGroupsByTeacher)
        Assert.assertEquals("ArrayList must contain 2 courses", 2, findAllGroupsByTeacher.size)

        val firstCourse = findAllGroupsByTeacher[0]

        Assert.assertEquals("First group must be ID: 2", 2L, firstCourse.id)
        Assert.assertEquals("Group with ID: 2 must have code: INZ006542L", "INZ006542L", firstCourse.code)

        val secondCourse = findAllGroupsByTeacher[1]

        Assert.assertEquals("First group must be ID:3", 3L, secondCourse.id)
        Assert.assertEquals("Group with ID: 3 must have code: INZ007263L", "INZ007263L", secondCourse.code)
    }

}