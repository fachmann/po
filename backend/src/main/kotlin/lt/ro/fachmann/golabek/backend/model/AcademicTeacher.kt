package lt.ro.fachmann.golabek.backend.model

import javax.persistence.*

/**
 * A didactic or didactic and academic employee of a university that runs Classes.
 */
@Entity
@Table(name = "g_teacher")
data class AcademicTeacher(
        @Id
        @GeneratedValue
        var id: Long = 0,

        /**
         * Specifies the professional title of a researcher
         */
        @Enumerated(EnumType.STRING)
        var degree: AcademicDegree? = null,

        var name: String = "",

        var surname: String = ""
)
