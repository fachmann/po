package lt.ro.fachmann.golabek.backend.model

import javax.persistence.*

/**
 * Its a series of lectures on a specific subject. With it's own program, [Group]s, set of [AcademicTeacher]
 */
@Entity
@Table(name = "g_course")
data class Course(
        @Id
        @GeneratedValue
        var id: Long = 0,

        var name: String = "",

        var code: String = "",

        @OneToMany(cascade = [CascadeType.ALL], orphanRemoval = true)
        @JoinColumn(name = "course_id")
        var groups: List<Group> = ArrayList()
)
