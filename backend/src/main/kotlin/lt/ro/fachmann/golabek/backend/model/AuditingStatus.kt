package lt.ro.fachmann.golabek.backend.model

/**
 * Current status of the hospitalization. The status can be: expected, rejected, accepted, completed, evaluated
 */
enum class AuditingStatus {
    COMPLEX,
    EVALUATED,
    ACCEPTED,
    REJECTED,
    FINISHED
}
