package lt.ro.fachmann.golabek.backend.controllers

import lt.ro.fachmann.golabek.backend.model.Plan
import lt.ro.fachmann.golabek.backend.model.Semester
import lt.ro.fachmann.golabek.backend.services.PlanService
import lt.ro.fachmann.golabek.backend.services.SemesterService
import lt.ro.fachmann.golabek.backend.util.PlanValidator
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.WebDataBinder
import org.springframework.web.bind.annotation.*
import javax.validation.Valid


/**
 * [PlanController] is responsible for managing with [Plan] entity
 */
@RestController
@RequestMapping("api/plan")
class PlanController {

    @Autowired
    lateinit private var planService: PlanService

    @Autowired
    lateinit private var semesterService: SemesterService


    @InitBinder
    protected fun initBinder(binder: WebDataBinder) {
        binder.validator = PlanValidator()
    }

    /**
     * POST: api/plan/save
     * <br>
     * Saves given [plan] in JSON format to a database.
     * @param plan Given [Plan] in POST body in JSON format
     * @return saved entity in JSON format
     */
    @RequestMapping(value = ["save"], method = [(RequestMethod.POST)])
    fun save(@Valid @RequestBody plan: Plan) = planService.save(plan)

    /**
     * GET: api/plan/plan/[id]
     * <br>
     * Goes to database for a specific [Plan] with given [id]
     * @param id Given id to compare with existing entity in repository
     * @return Searched [Plan] in JSON format or Error code 404
     */
    @RequestMapping(value = ["plan/{id}"], method = [(RequestMethod.GET)])
    fun getPlan(@PathVariable id: Long): Plan = planService.getOne(id)

    /**
     * GET: api/plan/plans
     * <br>
     * Goes to database for all [Plan]s in database
     * @return all [Plan]s in repository in JSON format
     */
    @RequestMapping(value = ["plans"], method = [(RequestMethod.GET)])
    fun findAllPlans() = planService.findAll()

    /**
     * GET: api/plan/semesters
     * <br>
     * Goes to database for all [Semester]s in database
     * @return all [Semester] in repository in JSON format
     */
    @RequestMapping(value = ["semesters"], method = [(RequestMethod.GET)])
    fun findAllSemesters() = semesterService.findAll()
}