package lt.ro.fachmann.golabek.backend.services

import lt.ro.fachmann.golabek.backend.model.Semester
import lt.ro.fachmann.golabek.backend.repositories.SemesterRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class SemesterService {

    @Autowired
    lateinit private var semesterRepository: SemesterRepository

    fun findAll(): MutableList<Semester> = semesterRepository.findAll()
}