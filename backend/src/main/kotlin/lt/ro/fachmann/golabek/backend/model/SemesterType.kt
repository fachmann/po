package lt.ro.fachmann.golabek.backend.model

/**
 * Determines if the semester is winter or summer
 */
enum class SemesterType {
    OTHER,
    WINTER,
    SUMMER
}
