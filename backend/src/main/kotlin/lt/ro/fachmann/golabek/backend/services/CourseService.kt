package lt.ro.fachmann.golabek.backend.services

import lt.ro.fachmann.golabek.backend.model.Course
import lt.ro.fachmann.golabek.backend.repositories.CourseRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class CourseService {

    @Autowired
    lateinit private var courseRepository: CourseRepository

    fun findAll(): MutableList<Course> = courseRepository.findAll()

    fun findAllCoursesByTeacher(teacherId: Long) = courseRepository.findByGroupsAcademicTeacherId(teacherId)
}