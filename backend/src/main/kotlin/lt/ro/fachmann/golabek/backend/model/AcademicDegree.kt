package lt.ro.fachmann.golabek.backend.model

/**
 * Enum class for describe [AcademicTeacher] degree
 */
enum class AcademicDegree {
    /**
     * inż.
     */
    ENGINEER,
    /**
     * mgr
     */
    MASTER,
    /**
     * dr
     */
    DOCTOR,
    /**
     * dr inż.
     */
    DOCTOR_ENGINEER,
    /**
     * dr hab.
     */
    HABILITATION,
    /**
     * prof. nadzw.
     */
    ASSOCIATE_PROFESSOR,
    /**
     * prof.
     */
    PROFESSOR
}
