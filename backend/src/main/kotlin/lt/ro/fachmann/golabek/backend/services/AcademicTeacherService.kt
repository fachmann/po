package lt.ro.fachmann.golabek.backend.services

import lt.ro.fachmann.golabek.backend.model.AcademicTeacher
import lt.ro.fachmann.golabek.backend.repositories.AcademicTeacherRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import javax.transaction.Transactional

@Service
class AcademicTeacherService {

    @Autowired
    lateinit private var academicTeacherRepository: AcademicTeacherRepository

    @Transactional
    fun bulkSave(academicTeacherList: List<AcademicTeacher>): List<AcademicTeacher> = academicTeacherRepository.save(academicTeacherList)

    fun findAll(): MutableList<AcademicTeacher> = academicTeacherRepository.findAll()

}