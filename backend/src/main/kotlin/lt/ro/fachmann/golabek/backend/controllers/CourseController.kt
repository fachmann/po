package lt.ro.fachmann.golabek.backend.controllers

import lt.ro.fachmann.golabek.backend.model.Course
import lt.ro.fachmann.golabek.backend.model.Group
import lt.ro.fachmann.golabek.backend.model.AcademicTeacher
import lt.ro.fachmann.golabek.backend.services.CourseService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod
import org.springframework.web.bind.annotation.RestController

/**
 * [CourseController] is responsible for managing with [Course] entity
 */
@RestController
@RequestMapping("api/course")
class CourseController {

    @Autowired
    lateinit private var courseService: CourseService

    /**
     * GET: api/course/courses
     * <br>
     * Goes to database for all [Course]s in database
     * @return all [Course] in repository in JSON format
     */
    @RequestMapping(value = ["courses"], method = [(RequestMethod.GET)])
    fun findAllGroups() = courseService.findAll()

    /**
     * GET: api/course/coursesByTeacherId/[teacherId]
     * <br>
     * Goes to database for all [Course]s that contains at least one [Group] leading by [AcademicTeacher] with given [teacherId]
     * @param teacherId Given [AcademicTeacher.id]
     * @return all [Course] in repository in JSON format
     */
    @RequestMapping(value = ["coursesByTeacherId/{teacherId}"], method = [(RequestMethod.GET)])
    fun findAllCoursesByTeacher(@PathVariable teacherId: Long) = courseService.findAllCoursesByTeacher(teacherId)
}