package lt.ro.fachmann.golabek.backend.model

enum class PlanStatus {
    /**
     * A hospitalization plan that has been approved for the given semester by the dean.
     */
    ACCEPTED,
    /**
     * Not verified plan
     */
    NOT_EXAMINED,
    REJECTED
}
