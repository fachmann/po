package lt.ro.fachmann.golabek.backend.repositories

import lt.ro.fachmann.golabek.backend.model.Semester
import org.springframework.data.jpa.repository.JpaRepository

interface SemesterRepository : JpaRepository<Semester, Long>
