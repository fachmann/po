package lt.ro.fachmann.golabek.backend.model

import javax.persistence.*

/**
 * The Committee of many visitors participating in the Hospit.
 */
@Entity
@Table(name = "g_committee")
data class Committee(
        @Id
        @GeneratedValue
        var id: Long = 0,

        @ManyToMany()
        var members: List<AcademicTeacher> = ArrayList(),

        /**
         * Hospitting President of the Commission who has the title of at least Doctor of Habilitation
         */
        @ManyToOne()
        var chairman: AcademicTeacher? = null
)
