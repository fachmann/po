package lt.ro.fachmann.golabek.backend.repositories

import lt.ro.fachmann.golabek.backend.model.Plan
import org.springframework.data.jpa.repository.JpaRepository

interface PlanRepository : JpaRepository<Plan, Long>
