package lt.ro.fachmann.golabek.backend.model

import javax.persistence.*

/**
 * A document that has information about hospitalization, data on its course and assessment of the course of classes.
 */
@Entity
class AuditingProtocol(
        id: Long = 0,

        documentNumber: String = "",

        @OneToMany(cascade = [(CascadeType.ALL)], orphanRemoval = true)
        @JoinColumn(name = "protocol_id")
        var grades: List<AuditingGrade>? = null,

        /**
         * The overall rating of the hospitalization where the Hospitalized is assessed is expressed as a real number between 1 and 5
         */
        var averangeGrade: Double = 0.0,

        @OneToOne(cascade = [(CascadeType.ALL)], orphanRemoval = true)
        var auditing: Auditing? = null
) : Document(id, documentNumber) {
    /**
     * Calculates grade based on self.grades.grade. Maps enum value lt.ro.fachmann.golabek.backend.model.Grade based on table
     *
     *
     * lt.ro.fachmann.golabek.backend.model.Grade type
     * Value
     * lt.ro.fachmann.golabek.backend.model.Grade::DISTINCTIVELY
     * 5
     * lt.ro.fachmann.golabek.backend.model.Grade::WELL
     * 4
     * lt.ro.fachmann.golabek.backend.model.Grade::ENOUGH
     * 3
     * lt.ro.fachmann.golabek.backend.model.Grade::INSUFFICIENTLY
     * 2
     * lt.ro.fachmann.golabek.backend.model.Grade::NOT_APPLICABLE
     * 0
     */
    fun overallGrade(): Double {
        val sum = grades?.map { auditingGrade ->
            when (auditingGrade.grade) {
                Grade.DISTINCTIVELY -> 5
                Grade.WELL -> 4
                Grade.ENOUGH -> 3
                Grade.INSUFFICIENTLY -> 2
                Grade.NOT_APPLICABLE -> 0
                else -> 0
            }
        }?.sum()?.toDouble()
        val count = grades?.count { auditingGrade -> auditingGrade.grade == Grade.NOT_APPLICABLE }
        return if (sum != null && count != null) sum / (14 - count) else 0.0
    }
}
