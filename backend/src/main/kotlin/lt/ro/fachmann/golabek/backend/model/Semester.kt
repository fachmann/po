package lt.ro.fachmann.golabek.backend.model

import javax.persistence.*

/**
 * Academic semester
 */
@Entity
@Table(name = "g_semester")
data class Semester(
        @Id
        @GeneratedValue
        var id: Long = 0,

        @Enumerated(EnumType.STRING)
        var type: SemesterType = SemesterType.OTHER,

        /**
         * The beginning year of the academic semester.
         */
        var beginYear: Int = 0,

        /**
         * The year of the end of the academic semester.
         */
        var endYear: Int = 0
)
