package lt.ro.fachmann.golabek.backend.controllers

import lt.ro.fachmann.golabek.backend.model.AcademicTeacher
import lt.ro.fachmann.golabek.backend.services.AcademicTeacherService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod
import org.springframework.web.bind.annotation.RestController

/**
 * [AcademicTeacherController] is responsible for managing with [AcademicTeacher] entity
 */
@RestController
@RequestMapping("api/teacher")
class AcademicTeacherController {

    @Autowired
    lateinit private var academicTeacherService: AcademicTeacherService

    /**
     * POST: api/teacher/save
     * <br>
     * Saves a [List] of [AcademicTeacher]s, [academicTeacherList] to repository
     * @param academicTeacherList Given list to save
     * @return saved list in JSON format
     */
    @RequestMapping(value = ["save"], method = [(RequestMethod.POST)])
    fun bulkSave(@RequestBody academicTeacherList: List<AcademicTeacher>) = academicTeacherService.bulkSave(academicTeacherList)

    /**
     * GET: api/teacher/teachers
     * <br>
     * Goes to database for all [AcademicTeacher]s in database
     * @return all [AcademicTeacher] in repository in JSON format
     */
    @RequestMapping(value = ["teachers"], method = [(RequestMethod.GET)])
    fun findAllTeachers() = academicTeacherService.findAll()
}