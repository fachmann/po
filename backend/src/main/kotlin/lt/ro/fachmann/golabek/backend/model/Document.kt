package lt.ro.fachmann.golabek.backend.model

import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.Id
import javax.persistence.Table

/**
 * Letter confirming some fact or thesis. It can be physical or electronic.
 */
@Entity
@Table(name = "g_document")
open class Document(
        @Id
        @GeneratedValue
        var id: Long = 0,

        var documentNumber: String = ""
)
