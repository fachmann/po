package lt.ro.fachmann.golabek.backend.model

import javax.persistence.*
import java.util.ArrayList
import java.util.Date

/**
 * The group consists of the name and the course code, the teacher, has the date and the room and the number of people saved.
 */
@Entity
@Table(name = "g_group")
data class Group(
        @Id
        @GeneratedValue
        var id: Long = 0,

        var code: String = "",

        var places: Int = 0,

        var totalPlaces: Int = 0,

        var room: String = "",

        var building: String = "",

        @ManyToOne()
        val academicTeacher: AcademicTeacher? = null,

        @ElementCollection(targetClass = Date::class)
        @CollectionTable(name = "g_group_date")
        val dates: List<Date> = ArrayList()
)