package lt.ro.fachmann.golabek.backend.model

/**
 * Grade of particular AuditingGrade without context.
 */
enum class Grade {
    /**
     * Value: 5
     */
    DISTINCTIVELY,
    /**
     * Value: 4
     */
    WELL,
    /**
     * Value: 3
     */
    ENOUGH,
    /**
     * Value: 2
     */
    INSUFFICIENTLY,
    /**
     * Value: 0
     */
    NOT_APPLICABLE
}
