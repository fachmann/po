package lt.ro.fachmann.golabek.backend.repositories

import lt.ro.fachmann.golabek.backend.model.Course
import org.springframework.data.jpa.repository.JpaRepository

interface CourseRepository : JpaRepository<Course, Long> {
    fun findByGroupsAcademicTeacherId(teacherId: Long): List<Course>
}
