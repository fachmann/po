package lt.ro.fachmann.golabek.backend.model

import javax.persistence.Entity
import javax.persistence.OneToOne

/**
 * A document, with a request to conduct hospitations conducted by the hospitalized in a specific course.
 */
@Entity
class AuditingCoverLetter(
        id: Long = 0,

        documentNumber: String = "",

        @OneToOne
        var auditing: Auditing? = null
) : Document(id, documentNumber)