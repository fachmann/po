package lt.ro.fachmann.golabek.backend.model

import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.Id
import javax.persistence.Table

/**
 * Substantive and methodological assessment included in the Hospitus Report. Has its assessment and thesis. The audited is assessed
 */
@Entity
@Table(name = "g_grade")
data class AuditingGrade(
        @Id
        @GeneratedValue
        var id: Long = 0,

        /**
         * The title of this o is evaluated in the given assessment. E.g. "Presentation of the subject, explanation of the purpose and scope of the classes".
         */
        var argument: String? = null,

        var grade: Grade? = null
)
