package lt.ro.fachmann.golabek.backend.util;

import lt.ro.fachmann.golabek.backend.model.Plan
import org.springframework.validation.Errors
import org.springframework.validation.Validator

class PlanValidator : Validator {

    override fun supports(clazz: Class<*>): Boolean {
        return Plan::class.java == clazz
    }

    override fun validate(obj: Any, errors: Errors) {
        val plan = obj as Plan
        for (auditing in plan.auditing) {
            if (auditing.audited == null) {
                errors.rejectValue("auditing", "auditing.audited.null")
            }
            if (auditing.committee == null) {
                errors.rejectValue("auditing", "auditing.committee.null")
            }
            if (auditing.course == null) {
                errors.rejectValue("auditing", "auditing.course.null")
            }
            if (auditing.audited != null && auditing.committee?.members != null) {
                if (auditing.audited.id == auditing.committee?.chairman?.id ||
                        auditing.committee?.members!!.count { academicTeacher -> academicTeacher.id == auditing.audited.id } > 0) {
                    errors.rejectValue("auditing", "auditing.audited.auditedInCommittee")
                }
                if (auditing.committee!!.members.count { academicTeacher -> academicTeacher.id == auditing.committee!!.chairman?.id } == 0) {
                    errors.rejectValue("auditing", "auditing.committee.chairman.chairmanNotInMembers")
                }
                if (auditing.committee!!.members.map { academicTeacher -> academicTeacher.id }.distinct().size != auditing.committee!!.members.size) {
                    errors.rejectValue("auditing", "auditing.committee.members.membersDuplicate")
                }
            }
        }
    }
}