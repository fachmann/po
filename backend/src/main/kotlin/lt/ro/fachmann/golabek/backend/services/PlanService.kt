package lt.ro.fachmann.golabek.backend.services

import lt.ro.fachmann.golabek.backend.model.Plan
import lt.ro.fachmann.golabek.backend.repositories.PlanRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class PlanService {

    @Autowired
    lateinit private var planRepository: PlanRepository

    fun save(plan: Plan): Plan = planRepository.save(plan)

    fun findAll(): MutableList<Plan> = planRepository.findAll()

    fun getOne(id: Long): Plan = planRepository.getOne(id)
}