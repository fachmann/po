package lt.ro.fachmann.golabek.backend.model

import com.fasterxml.jackson.annotation.JsonBackReference
import javax.persistence.*
import java.util.Date

/**
 * Participation of the Visitor in the Hospitowany classes, evaluation of the quality of teaching.
 */
@Entity
@Table(name = "g_auditing")
data class Auditing(
        @Id
        @GeneratedValue
        var id: Long = 0,

        var dateOfCompletion: Date? = null,

        @Enumerated(EnumType.STRING)
        var status: AuditingStatus = AuditingStatus.COMPLEX,

        @ManyToOne()
        var course: Course? = null,

        /**
         * Academic teacher conducting classes who are hospitalized
         */
        @ManyToOne()
        val audited: AcademicTeacher? = null,

        @OneToOne(cascade = [(CascadeType.ALL)], orphanRemoval = true)
        var committee: Committee? = null,

        @OneToOne(cascade = [(CascadeType.ALL)], orphanRemoval = true)
        var auditingProtocol: AuditingProtocol? = null,

        @OneToOne(cascade = [(CascadeType.ALL)], orphanRemoval = true)
        var auditingCoverLetter: AuditingCoverLetter? = null
)
