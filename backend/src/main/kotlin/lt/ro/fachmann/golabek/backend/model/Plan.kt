package lt.ro.fachmann.golabek.backend.model

import javax.persistence.*

/**
 * Document, Performed by the Dean's Plenipotentiaries for study fields after 15.10 and until 23.10 for the winter semester and after 1.03 and until 8.03 for the summer semester. Applies to a given field of study, academic year and semester. It has a list of courses and a list of members of the hospitalization team.
 */
@Entity
class Plan(
        id: Long = 0,

        documentNumber: String = "",

        /**
         * A hospitalization plan status.
         */
        @Enumerated(EnumType.STRING)
        var status: PlanStatus = PlanStatus.NOT_EXAMINED,

        @OneToMany(cascade = [CascadeType.ALL], orphanRemoval = true)
        @JoinColumn(name = "plan_id")
        var auditing: List<Auditing> = ArrayList(),

        @ManyToOne()
        var semester: Semester? = null
) : Document(id, documentNumber)
