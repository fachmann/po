package lt.ro.fachmann.golabek.backend.repositories

import lt.ro.fachmann.golabek.backend.model.AcademicTeacher
import org.springframework.data.jpa.repository.JpaRepository

interface AcademicTeacherRepository : JpaRepository<AcademicTeacher, Long>
